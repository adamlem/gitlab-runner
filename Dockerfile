FROM gitlab/gitlab-runner:alpine-v13.1.1
COPY config-template.toml /
ENV TEMPLATE_CONFIG_FILE /config-template.toml
